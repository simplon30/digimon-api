import { Router, useRouter } from "next/router";
import { useEffect, useState } from "react";

interface Digimon {
  content: Content[]
  pageable: Pageable
}

interface Pageable {
  nextPage: string
}

interface Content {
  id?: number;
  name: string;
  image: Image;
  href: string;
}

interface Image {
  href: string;
}

export default function Home() {

  const router = useRouter()
  const [id, setId] = useState<number>(0);
  const [digimon, setDigimon] = useState<Digimon>();

  useEffect(() => {
    fetch(`https://digimon-api.com/api/v1/digimon?page=${id}`)
      .then(response => response.json())
      .then((data) => {
        setDigimon(data)
      })
  }, [id])

  function add() {
    setId(id => {
      return id + 1;
    })
  }

  function remove() {
    setId(id => {
      return id - 1;
    })
  }

  function texte(texte : string) {
    let txt = texte.replace(/ /g, '_');
    router.push(`https://wikimon.net/${txt}`)
  }

  return (
    <>
      <h1>Attraper les tous !</h1>

      <div className="d-flex justify-content-evenly pt-3 pb-3">
        <button className="btn btn-primary" onClick={() => remove()}>page précédente</button>
        <p className="card-title">Page : {id}</p>
        <button className="btn btn-primary" onClick={() => add()}>page suivante</button>
      </div>

      <div className="container">
        <div className="row d-flex justify-content-center">

          {digimon?.content.map((item) =>
            <>
              <div className="card m-1" style={{ width: "22rem" }}>
                <img src={`${item.image}`} className="card-img-top" alt="..." />
                <div className="card-body">
                  <h5 className="card-title">{item.name}</h5>
                  <a href={`https://wikimon.net/${item.name}`} className="btn btn-primary" target={"_blank"}>Voir Plus</a>
                  <button className="btn btn-primary" onClick={() => texte()}>info</button>
                </div>
              </div>

            </>
          )}

        </div>
      </div>

    </>
  )
}